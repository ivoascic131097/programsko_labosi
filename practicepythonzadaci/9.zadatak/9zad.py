import random

actual = random.randint(1,9)
c = 0
while True:
    guess = input("Enter num: \n")
    if guess == "exit":
        break
    else:
        c += 1
        guess = int(guess)
    if guess == actual:
        print ("you got it! in  " + str(c) +" tries")
        break
    print ("actual is " +("lower" if guess > actual else "higher") + "\n")
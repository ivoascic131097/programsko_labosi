from random import sample


def tic_tac_toe():
    board = []
    s = 3
    win = False

    for x in range(s):
        y = sample(range(s), s)
        board.append(y)
        print(y)


    #horizontal check:
    for x in range(s):
        if (board[x][0] == board[x][1] == board[x][2]) and board[x][0] != 0:
            print("Player {} wins!".format(board[x][0]))
            win = True

    #vertical check:
    for x in range(s):
        if (board[0][x] == board[1][x] == board[2][x]) and board[0][x] != 0:
            print("Player {} wins!".format(board[0][x]))
            win = True

    #diagonal_check:
    if (board[0][0] == board[1][1] == board[2][2]) and board[1][1] != 0:
        print("Player {} wins!".format(board[1][1]))
        win = True
    elif (board[0][2] == board[1][1] == board[2][0]) and board[1][1] != 0:
        print("Player {} wins!".format(board[1][1]))
        win = True

    if not win:
        print("Draw..")

tic_tac_toe()
from bs4 import BeautifulSoup
import requests
import re

def allstories(f):

    page=requests.get(f)
    soup=BeautifulSoup(page.text,"html.parser")

    print("STORIES CURRENTLY ON NYTIMES.COM (in order of appearance):\n")
    for art in soup.find_all("h2", class_="story-heading"):
        arttitle=art.string
        if arttitle is not None:
            t=str(arttitle).strip()
            print(t)

allstories("http://www.nytimes.com")
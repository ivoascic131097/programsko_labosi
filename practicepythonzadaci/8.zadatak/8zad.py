a = input("player A: ")
b = input("player B: ")
win = {"P":"R","R":"S","S":"P"}

if not (a in win and b in win):
    print ("Invalid input")
elif a == b:
    print ("No winner")
else:
    print ("Winner is player " + "A" if win[a] == b else "B")
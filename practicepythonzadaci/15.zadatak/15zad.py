def reverseWord(w):
  return ' '.join(w.split()[::-1])

  #.split() funkcija razdvaja svaku riječ u stringu u listu, 
  #[::-1] postavlja granice ovisno koja je prva i zadnja dok -1 označava da se krećemo sa kraja
  #.join() sve elemente sa tuplea spaja u jedan strin 

word="Ana voli milovana"

print(reverseWord(word))
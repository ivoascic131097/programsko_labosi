def guess_work():  
    
    count = 0
    min = 0
    max = 101
    ans = ""
    
    while ans!='same':
        count+=1
        mid = (max + min)//2
        
        ans = input("My guess is %s. Is that high, low or same? "%(mid))
       
        if ans=='high':
            max = mid
               
        if ans=='low':
            min = mid
            
        if ans=="":
            return 50
    
    else:
        print ("Congrats to me! I guessed it in %s tries."%(count))
        
if __name__=="__main__":
    
    print("Guess a number between 0 and 100 and tell whether high or low when prompted!")
    guess_work()
  
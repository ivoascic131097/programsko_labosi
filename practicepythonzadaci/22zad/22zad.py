with open("DarthLukeLeia.txt", "r") as names:
    count_names = dict()
    for line in names:
        if line.replace("\n", "") in count_names.keys():
            count_names[line.replace("\n", "")] += 1
        else:
            count_names[line.replace("\n", "")] = 1

    print(count_names)

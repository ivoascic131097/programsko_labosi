grades = { "Pero": [2,3,3,4,3,5,3],
           "Djuro" : [4,4,4],
           "Marko" : [3,3,2,3,5,1]
           }
def avg(grade):
    total=float(sum(grade))
    return total/len(grade)

maxN = None
maxG = 0
for N,G in grades.items():
    if maxN is None:
        maxN=N
        maxG=avg(G)
    elif maxG < avg(G):
        maxN=N
        maxG=avg(G)

print (maxN, maxG)

